package com.vontobel;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}
